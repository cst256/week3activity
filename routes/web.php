<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\Controller;
/*
 |--------------------------------------------------------------------------
 | Web Routes
 |--------------------------------------------------------------------------
 |
 | Here is where you can register web routes for your application. These
 | routes are loaded by the RouteServiceProvider within a group which
 | contains the "web" middleware group. Now create something great!
 |
 */

Route::get('/', function () {
    return view('welcome');
});
    
    Route::get('/login', function () {
        return view('login');
    });
        
        Route::post('/dologin', [LoginController::class, 'index']);
        
        Route::get('/login2', function () {
            return view('login2');
        });
            
        Route::get('addcustomer/{firstName}/{lastName}', [Controller::class, 'addCustomer']);
        Route::get('addorder/{product}/{customer_id}', [Controller::class, 'addOrder']);
        Route::get('createorder/{firstName}/{lastName}/{product}', [Controller::class, 'createOrder']);