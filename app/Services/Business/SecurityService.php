<?php
namespace App\Services\Business;

use App\Models\UserModel;
use App\Services\Data\SecurityDAO;

class SecurityService
{
    public function login(UserModel $userModel){
         $loginHandler = new SecurityDAO();
         
         $loginResults = $loginHandler->findByUser($userModel);
         
         if ($loginResults){
             session(['loggedIn' => TRUE]);
         }
         //Attempt to login
         return $loginHandler->findByUser($userModel);
    }
    
    //Check to see if there is an authenticated user
    public function authenticated(){
        if ((session('loggedIn') == null)){
            return false;
        }
        return session('loggedIn');
    }
}

