<?php
namespace App\Services\Business;
 use App\Services\Data\CustomerDAO;
 use App\Services\Data\OrderDAO;

 class OrderService{
    public function createOrder($firstName, $lastName, $product){
        $conn = $this->connectToDatabase();
        if ($conn == false){
            //Stop execution
            return;
        }
        mysqli_autocommit($conn, FALSE);
        mysqli_begin_transaction($conn);
        
        $id = (new CustomerDAO())->addCustomer($firstName, $lastName, $conn);
        mysqli_commit($conn);
        
        mysqli_begin_transaction($conn);
        (new OrderDAO())->addOrder($product, $id, $conn);
        mysqli_commit($conn);
    }
    public function connectToDatabase(){
        $conn = mysqli_connect("localhost", "root", "root", "cst256temp");
        
        if (mysqli_connect_error()){
            echo "Unable to connect to the database";
            return false;
        }
        return $conn;
    }
}