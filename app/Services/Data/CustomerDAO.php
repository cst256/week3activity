<?php
namespace App\Services\Data;

class CustomerDAO
{
    public function addCustomer($firstName, $lastName, $conn){
        //Insert first name and last name
        
       
        
        
        $sql = "INSERT INTO `customer` (FIRST_NAME, LAST_NAME) VALUES (?, ?) ";
        $query = mysqli_prepare($conn, $sql);
        $query->bind_param("ss", $firstName, $lastName);
        
        $results = $query->execute();
        
       
        if (mysqli_error($conn)){
            echo "Error saving data to database";
        } else {
            echo "SAVED TO DATABASE";
        }
        
        $find_id = mysqli_prepare($conn, "SELECT * FROM `customer` WHERE (FIRST_NAME = ? AND LAST_NAME = ?) ORDER BY ID DESC LIMIT 1");
        $find_id->bind_param("ss", $firstName, $lastName);
        $find_id->execute();
        $find_id_results = $find_id->get_result();
        

        $row = mysqli_fetch_array($find_id_results);
        
        return $row[0];
    }
}
