<?php

namespace App\Http\Controllers;
use App\Models\UserModel;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use App\Services\Business\SecurityService;

class LoginController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function index(Request $request){
        $username = $request->input('username');
        $password = $request->input('password');
        
        //Validate form data
        $this->validateForm($request);
        
        $usrModel = new UserModel();
        $usrModel->setUsername($username);
        $usrModel->setPassword($password);
        $ss = new SecurityService();
        $ss->login($usrModel);
        
        if ($ss->authenticated()){
            return view('loginPassed2')->with('model', $usrModel);
        } else {
            return view('loginFailed');
        }
        
        echo "The username passed was ".$username." and the password passed was ".$password;
        echo "<br>";
    }
    private function validateForm(Request $request){
        //Setup data validation rules
        $rules = ['username' => 'Required | Between:4,10 | Alpha', 
            'password' => 'Required | Between:4, 10'];
        
        //Run rules
        $this->validate($request, $rules);
    }
}
