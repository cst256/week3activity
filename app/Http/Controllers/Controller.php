<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Services\Data\CustomerDAO;
use App\Services\Data\OrderDAO;
use App\Services\Business\OrderService;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function addCustomer($firstName, $lastName){
        $customerManager = new CustomerDAO();
        $customerManager->addCustomer($firstName, $lastName);
    }
    public function addOrder($product, $customer_id){
        $orderManager = new OrderDAO();
        $orderManager->addOrder($product, $customer_id);
    }
    public function createOrder($firstName, $lastName, $product){
        (new OrderService())->createOrder($firstName, $lastName, $product);
    }
}
