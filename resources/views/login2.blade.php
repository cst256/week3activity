<html>
@extends('layouts.appmaster') 
@section('title', 'Login Page')
@section('content')
<h1>Login!</h1>
<form action='dologin' method='POST'>
	<input type='hidden' name='_token' value='<?php echo csrf_token()?>' />
	
	<input type='text' name='username'/><?php echo $errors->first('username');?>
	<br> 
	
	<input type='password' name='password'/><?php echo $errors->first('password');?>
	<br> 
	<input type='submit' value='Log In!' />
</form>

@endsection
</html>